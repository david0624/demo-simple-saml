package pers.apricot.config

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@CompileStatic
@Configuration
class InterceptorConfig implements WebMvcConfigurer {

    String[] excludePattern = ['/', "/saml/**"]

    @Autowired
    LoginValidateInterceptor loginValidateInterceptor

    @Override
    void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginValidateInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(excludePattern)
    }
}
