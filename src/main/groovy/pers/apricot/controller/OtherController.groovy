package pers.apricot.controller

import groovy.transform.CompileStatic
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@CompileStatic
@Controller
@RequestMapping("private")
class OtherController {

    @GetMapping("aaa")
    String aaa(Model model) {
        model.addAttribute("sign", "aaa")
        return "private/aaa.html"
    }
}
