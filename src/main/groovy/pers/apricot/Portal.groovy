package pers.apricot

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Portal {
    static void main(String[] args) {
        SpringApplication.run(Portal, args)
    }
}
