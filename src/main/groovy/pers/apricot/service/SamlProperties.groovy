package pers.apricot.service

import groovy.transform.CompileStatic
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@CompileStatic
@ConfigurationProperties(prefix = "saml")
@Component
class SamlProperties {

    /**
     * 默认不开启
     */
    boolean enabled = false
    /**
     * 依赖方标识符,即entityId, 如 urn:ttx:demo
     */
    String relyingPartyIdentifier = ''
    /**
     * 断言消费者服务的地址 如 https://secure.ite.pepsico.com/app/mypepsico_ttxqawms_1/exkulqw48obTtaNtd0h7/sso/saml 也可以从meta中获取,可以不填
     */
    String assertionConsumerServiceUrl = ''
    /**
     *  metadata,及xml配置文件,里面记录了各种配置,如果开启,则必填
     */
    String metadataLocation

    /**
     * saml-public-key.crt 签名使用
     */
    String publicKeyCrt

    /**
     * saml-private-key.pk8 签名使用
     */
    String privateKeyPk8

}
