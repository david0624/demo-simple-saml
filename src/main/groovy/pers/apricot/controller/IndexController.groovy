package pers.apricot.controller

import groovy.transform.CompileStatic
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@CompileStatic
@Controller
class IndexController {
    @GetMapping("/")
    String index(Model model) {
        model.addAttribute("name", "david")
        return "index.html"
    }
}
